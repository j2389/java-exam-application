/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author JM Lekhase
 */
public class StudentDA {

    private static ArrayList<Student> arStud;
    private static final String studentFile = "students.dat";

    public static void initialise() throws DataStorageException {
        try {
            FileInputStream fis = new FileInputStream(studentFile);
            ObjectInputStream objRead = new ObjectInputStream(fis);
            arStud = (ArrayList<Student>) objRead.readObject();
            objRead.close();
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No data in the file " + ex.getMessage());

            arStud = new ArrayList<>();
        } catch (IOException ex) {
            throw new DataStorageException("No data to read " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//Initializing the file

    public static void terminate() throws DataStorageException {
        try {
            FileOutputStream fos = new FileOutputStream(studentFile);
            ObjectOutputStream objWrite = new ObjectOutputStream(fos);
            objWrite.writeObject(arStud);
            objWrite.close();
        } catch (IOException ex) {
            throw new DataStorageException("Cannot write " + ex.getMessage());
        }
    }//Closing Application

    public static void addNewStud(Student st) throws DuplicateException {
        boolean duplicate = false;
        for (int x = 0; x < arStud.size() && !duplicate; x++) {
            if ((arStud.get(x).getStudNo().equals(st.getStudNo())) && (arStud.get(x).getSubject().equals(st.getSubject()))) {
                duplicate = true;
            }
        }
        if (duplicate) {
            throw new DuplicateException(st.getSubject() + ": Student already enroll for this subject");
        } else {
            arStud.add(st);
        }
    }//adding new students

    public static void updatePhone(Student ath, String newPhone) throws NotFoundException {
        boolean found = false;
        String aStud = ath.getStudNo();
        for (int x = 0; x < arStud.size() && !found; x++) {
            if (aStud.equals(arStud.get(x).getStudNo())) {
                found = true;
                arStud.get(x).setPhone(newPhone);
            }
        }
        if (!found) {
            throw new NotFoundException(aStud + " Student not found ");
        }
    }//Upading Student Phone numbers

    public static Student findStudRecord(String aStud) throws NotFoundException {
        boolean found = false;
        Student stud = null;
        for (int x = 0; x < arStud.size() && !found; x++) {
            stud = arStud.get(x);
            if (aStud.equals(stud.getStudNo())) {
                found = true;
            }
        }
        if (found) {
            return stud;
        } else {
            throw new NotFoundException(aStud + " Student not found ");
        }
    }//looking for student records using Student number

    public static void deleteSubject(Student st, String aSubj) throws NotFoundException {
        boolean found = false;
        String aStud = st.getStudNo();
        for (int x = 0; x < arStud.size() && !found; x++) {
            if ((arStud.get(x).getStudNo().equals(st.getStudNo())) && (arStud.get(x).getSubject().equals(aSubj))) {
                found = true;
                arStud.remove(x);
            }
        }
        if (!found) {
            throw new NotFoundException("Student not found " + aStud);
        }
    }//Deleting a Module using student No

    public static ArrayList<Student> getAll() {
        return arStud;
    }

    public static ArrayList<Student> getAllStudent(String aSubj) {
        ArrayList arrAth = new ArrayList();
        for (Student oStud : arStud) {
            if (oStud.getSubject().equals(aSubj)) {
                arrAth.add(oStud);

            }
        }
        return arrAth;
    }

    public static Integer count() {
        int cnt = 0;
        for (int i = 0; i < arStud.size(); i++) {
            if (arStud.get(i).getStudNo().equalsIgnoreCase("Student No")) {
                cnt += 1;
            }
        }

        return cnt;
    }

}
