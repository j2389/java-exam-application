/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lekhase.gui;

import lekhase.data.Student;
import lekhase.data.DuplicateException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author JM Lekhase
 */
public class StudentRegistration extends JFrame {

    private ArrayList<Student> arStud;
    JTextField txtNm = new JTextField(25);
    JTextField txtPhone = new JTextField(25);
    JTextField txtStNo = new JTextField(25);
    JComboBox<String> cmbSubject = new JComboBox<>();
    JComboBox<String> cmbCourse = new JComboBox<>();
    JRadioButton rbMale = new JRadioButton("Male");
    JRadioButton rbFemale = new JRadioButton("Female");

    JLabel lblStNo = new JLabel("Student No:");
    JLabel lblName = new JLabel("Name:");
    JLabel lblPhone = new JLabel("Phone No:");
    JLabel lblGender = new JLabel("Gender:");
    JLabel lblCourse = new JLabel("Course Code:");
    JLabel lblSubject = new JLabel("Subject Code:");
    JButton btnSelect = new JButton("Select Course");
    JButton btnAdd = new JButton("Enroll Student");
    JButton btnClear = new JButton("Clear");
    JButton btnClose = new JButton("Close");

    public StudentRegistration(ArrayList<Student> arStud) {
        this.arStud = arStud;
        JPanel pnl = new JPanel();
        pnl.setLayout(null);
        pnl.add(txtNm);
        pnl.add(txtStNo);
        pnl.add(txtPhone);

        pnl.add(cmbSubject);
        pnl.add(cmbCourse);
        pnl.add(rbMale);
        pnl.add(rbFemale);

        pnl.add(lblStNo);
        pnl.add(lblName);
        pnl.add(lblPhone);
        pnl.add(lblGender);
        pnl.add(lblCourse);
        pnl.add(lblSubject);
        pnl.add(btnAdd);
        pnl.add(btnClear);
        pnl.add(btnClose);
        pnl.add(btnSelect);

        lblStNo.setBounds(10, 20, 140, 20);
        lblName.setBounds(10, 50, 140, 20);

        lblPhone.setBounds(10, 80, 140, 20);
        lblGender.setBounds(10, 110, 140, 20);
        lblCourse.setBounds(10, 140, 140, 20);
        lblSubject.setBounds(10, 170, 140, 20);
        //input
        txtStNo.setBounds(100, 20, 120, 20);
        txtNm.setBounds(100, 50, 120, 20);
        //radiobuttons in same line
        txtPhone.setBounds(100, 80, 120, 20);
        rbMale.setBounds(100, 110, 100, 20);
        rbFemale.setBounds(210, 110, 100, 20);
        //input
        cmbCourse.setBounds(100, 140, 120, 20);
        //couse btn
        btnSelect.setBounds(220, 140, 120, 20);
        cmbSubject.setBounds(100, 170, 120, 20);

        btnAdd.setBounds(10, 210, 120, 20);
        btnClear.setBounds(150, 210, 120, 20);
        btnClose.setBounds(280, 210, 120, 20);

        setContentPane(pnl);
        btnAdd.addActionListener(new btnAddEvent());
        btnClear.addActionListener(new btnClearEvent());
        btnClose.addActionListener(new btnCloseEvent());
        btnSelect.addActionListener(new btnSelectEvent());

        getCourses();
        getsubjects();

    }

    private class btnAddEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                Student objAth = getStudents();
                objAth.addNewStud();
            } catch (DuplicateException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }

    private class btnClearEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            txtNm.setText("");
            txtStNo.setText("");
            txtNm.setText("");
            txtPhone.setText("");
            cmbCourse.setSelectedIndex(0);
            cmbSubject.setSelectedItem(null);
            cmbSubject.addItem("");
            txtStNo.requestFocus();
            rbMale.setSelected(true);
            rbFemale.setSelected(false);

        }

    }

    private class btnSelectEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {

            String rLine;
            String[] arsp;

            try {
                Scanner scRd = new Scanner(new FileReader("subjects.txt"));
                while (scRd.hasNextLine()) {
                    rLine = scRd.nextLine();
                    arsp = rLine.split(",");
                    if (cmbCourse.getSelectedIndex() == 0) {
                        switch (arsp[0]) {
                            case "IT100":
                                cmbSubject.addItem(arsp[1]);
                                break;
                        }

                    } else if (cmbCourse.getSelectedIndex() == 1) {
                        switch (arsp[0]) {
                            case "CS100":
                                cmbSubject.addItem(arsp[1]);
                                break;
                        }

                    }
                }

                scRd.close();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Error reading Subject file: " + ex.getMessage());
            }

        }

    }

    private class btnCloseEvent implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {

            dispose();

        }

    }

//get inputs
    public Student getStudents() {
        String stno, name, phone, gender, cos, subj;

        Student objStud = null;

        try {
            if (rbMale.isSelected()) {
                gender = rbMale.getText();
                cos = cmbCourse.getSelectedItem().toString();
                subj = cmbSubject.getSelectedItem().toString();
                stno = txtStNo.getText();
                name = txtNm.getText();
                phone = txtPhone.getText();
            } else {
                gender = rbFemale.getText();
                cos = cmbCourse.getSelectedItem().toString();
                subj = cmbSubject.getSelectedItem().toString();
                stno = txtStNo.getText();
                name = txtNm.getText();
                phone = txtPhone.getText();
            }

            objStud = new Student(stno, name, phone, gender, cos, subj);
        } catch (InputMismatchException e) {
            System.out.println(e.getMessage());
        }
        return objStud;
    }

    public void getCourses() {
        String rLine;
        String[] arsp;

        try {
            Scanner scRd = new Scanner(new FileReader("courses.txt"));
            while (scRd.hasNextLine()) {
                rLine = scRd.nextLine();
                arsp = rLine.split(",");
                cmbCourse.addItem(arsp[0]);
            }
            scRd.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error reading Course file: " + ex.getMessage());
        }
    }

    public void getsubjects() {

        String rLine;
        String[] arsp;

        try {
            Scanner scRd = new Scanner(new FileReader("subjects.txt"));
            while (scRd.hasNextLine()) {
                rLine = scRd.nextLine();
                arsp = rLine.split(",");
                if (cmbCourse.getSelectedIndex() == 0) {
                    switch (arsp[0]) {
                        case "IT100":

                            cmbSubject.addItem(arsp[1]);
                            break;
                    }
                }
            }

            scRd.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error reading Subject file: " + ex.getMessage());
        }
    }
}
